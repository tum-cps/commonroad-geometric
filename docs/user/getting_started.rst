.. _getting_started:

============================
Getting Started
============================

The easiest way of getting familiar with the framework is to consult the `tutorials <https://github.com/CommonRoad/crgeo/tree/main/tutorials>`_ directory, which contains a multitude of simple application demos that showcase the intended usage of the package.